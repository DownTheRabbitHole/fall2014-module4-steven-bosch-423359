#!/usr/bin/env python
import sys, os, re, operator

regex = re.compile(r"([\w ]*)batted (\d+) times with (\d+) hits and (\d+) runs")

def getStats(test):
    match = regex.match(test)
    if match is not None:
        return (match.group(1), match.group(2), match.group(3), match.group(4))
    else:
        return (False, False, False, False)

if len(sys.argv) < 2:
    sys.exit("Usage: %s <filepath>" % sys.argv[0])

file = sys.argv[1]

if not os.path.exists(file):
    sys.exit("Error: File '%s' not found" % sys.argv[1])

f = open(file)

playerDict = {}
for line in f:
    name, atBats, hits, runs = getStats(line)
    if name != False and hits != False and atBats != False and runs != False:
        if playerDict.has_key(name):
            newHits = float(hits) + float(playerDict[name][1])
            newAtBats = float(atBats) + float(playerDict[name][0])
            playerDict[name] = (newAtBats, newHits)
        else:
            playerDict[name] = (atBats, hits)
    
f.close()

playerStats = {}
for name, val in playerDict.items():
    playerStats[name] = val[1]/val[0]

sorted_players = sorted(playerStats.items(), key=operator.itemgetter(1), reverse=True)
for player, avg in sorted_players:
    print "%s: %s" % (player, round(avg, 3))

